import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlPolitics extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.only(bottom: 85, top: 10),
        child: Text.rich(
          TextSpan(
            text: 'Нажимая кнопку “Далее”, вы принимете условия\n',
            style: TextStyle(color: Color(0xff979797), fontSize: 13),
            children: <TextSpan>[
              TextSpan(
                text: 'Пользовательского соглашения',
                style: TextStyle(decoration: TextDecoration.underline),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    await launch("https://faem.ru/legal/agreement");
                  },
              ),
              TextSpan(
                text: ' и ',
              ),
              TextSpan(
                text: 'Политики\nконфиденцальности',
                style: TextStyle(decoration: TextDecoration.underline),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    await launch("https://faem.ru/privacy");
                  },
              ),
            ],
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
