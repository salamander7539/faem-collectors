import 'package:faem_collectors/core/utils/color.dart';
import 'package:faem_collectors/features/authorization_phone/controllers/button_color_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Button extends StatelessWidget {

  final phoneFilledController = Get.put(ButtonColorController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: GetX<ButtonColorController>(

        builder: (controller) {
          return FlatButton(
            child: Text(
              'Далее',
              style: TextStyle(
                fontSize: 18.0,
                color: AppColor.white,
              ),
            ),
            color: controller.isPhoneFilled.value,
            splashColor: AppColor.darkBlue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            padding: EdgeInsets.only(left: 130, top: 20, right: 130, bottom: 20),
            onPressed: () {},
          );
        }
      ),
    );
  }
}
