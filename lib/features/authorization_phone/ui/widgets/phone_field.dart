import 'package:faem_collectors/core/utils/color.dart';
import 'package:faem_collectors/features/authorization_phone/controllers/button_color_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:get/get.dart';

var phoneController = new MaskedTextController(mask: '+7 000 000-00-00');

class PhoneField extends StatefulWidget {
  @override
  _PhoneFieldState createState() => _PhoneFieldState();
}

class _PhoneFieldState extends State<PhoneField> {

  final phoneFilledController = Get.put(ButtonColorController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    phoneController.text = '';
    phoneController.afterChange = (String previous, String next) {
      if (next.length > previous.length) {
        phoneController.selection = TextSelection.fromPosition(
            TextPosition(offset: phoneController.text.length));
      }
      return false;
    };
    phoneController.beforeChange = (String previous, String next) {
      if (phoneController.text == '8') {
        phoneController.updateText('+7 ');
      }
      return true;
    };
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(40.0),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 7,
                offset: Offset(0, 0), // changes position of shadow
              ),
            ],
          ),
          height: 100.0,
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0),
                  ),
                  color: AppColor.darkBlue,
                ),
                height: 50.0,
                child: Center(
                  child: Text(
                    'Укажите ваш номер телефона',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                  ),
                  color: AppColor.white,
                ),
                height: 50.0,
                child: TextField(
                  autofocus: true,
                  controller: phoneController,
                  style: TextStyle(fontSize: 20),
                  maxLength: 16,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    fillColor: Colors.white,
                    counterText: '',
                    contentPadding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.195,
                    ),
                    hintStyle: TextStyle(
                      color: Color(0xFFC4C4C4),
                    ),
                    hintText: '+7 918 888-88-88',
                  ),
                  onChanged: (String value) {
                    if (value == '+7 8') {
                      phoneController.text = '+7';
                    }
                    phoneFilledController.watchPhone();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
