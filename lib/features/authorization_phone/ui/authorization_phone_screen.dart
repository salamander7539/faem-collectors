import 'package:faem_collectors/core/utils/color.dart';
import 'package:faem_collectors/features/authorization_phone/ui/widgets/button.dart';
import 'package:faem_collectors/features/authorization_phone/ui/widgets/phone_field.dart';
import 'package:faem_collectors/features/authorization_phone/ui/widgets/url.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthPhoneScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: AppColor.white10,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: Icon(
            Icons.arrow_back_ios,
            color: AppColor.black,
          ),
        ),
      ),
      body: Align(
        widthFactor: 1.0,
        heightFactor: 1.0,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: PhoneField(),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: UrlPolitics(),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: Button(),
            ),
          ],
        ),
      ),
    );
  }
}
