import 'package:faem_collectors/core/utils/color.dart';
import 'package:faem_collectors/features/authorization_phone/ui/widgets/phone_field.dart';
import 'package:get/state_manager.dart';

class ButtonColorController extends GetxController {
  var isPhoneFilled = AppColor.lightGray.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    watchPhone();
  }

  void watchPhone() {
    if (phoneController.text.length == 16) {
      isPhoneFilled.value = AppColor.darkBlue;
    } else {
      isPhoneFilled.value = AppColor.lightGray;
    }
  }
}