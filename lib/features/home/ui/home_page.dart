import 'package:faem_collectors/features/drawer/ui/main_drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MainDrawer(),
      appBar: AppBar(
        // leading: IconButton(
        //   icon: Icon(Icons.menu),
        //   onPressed: () {
        //     _scaffoldKey.currentState.openDrawer();
        //   },
        // ),
        title: Text('CollectorsApp'),
        centerTitle: true,
      ),
      body: Stack(
        children: [],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: null,
        label: Text('Start'),
      ),
    );
  }
}
