import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color black = Color(0xff000000);
  static const Color white = Color(0xffffffff);
  static const Color white10 = Color(0x1affffff);

  static const Color darkBlue = Color(0xff40475A);
  static const Color lightBlue = Color(0xffF3F4F8);

  static const Color lightGray = Color(0xffe6e6e6);

  static const Color lightGray30 = Color(0xffb3b3b3);

  static const Color lightGray2 = Color(0xffB8B8B8);
}